
export function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

export function showPreview(value) {
  if (typeof value === 'object') {
    return value.preview;
  }

  return value;
};

export const formReducer = (state, value) => {
  return ({
    ...state,
    ...value
  });
};

export const hasProperty = (obj, type) => obj.hasOwnProperty(type);

export const getValidationDate = () => {
  const dt = new Date();
  const yr = dt.getFullYear().toString().slice(-2);
  const mt = (dt.getMonth() + 1).toString().padStart(2, "0");

  return yr + '-' + mt;
};

export const cleaveOptions = {
  cardNumber: {
    delimiters: [' - ', ' - ', ' - '],
    blocks: [4, 4, 4, 4],
    creditCard: true
  },
  expiry: {
    date: true,
    datePattern: ['m', 'y'],
    dateMin: getValidationDate(),
    delimiters: [' / ']
  }
};
