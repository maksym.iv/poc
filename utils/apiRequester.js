import Axios from 'axios';

const axios = Axios.create({
  baseURL: process.env.API_HOST,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  },
  responseType: 'json',
  timeout: 5000
});

export function get(url, data = {}, options = {}) {
  return axios.get(url, { params: data, ...options });
}

export function post(url, data = {}, options = {}) {
  return axios.post(url, data, options);
}

export function put(url, data = {}, options = {}) {
  return axios.put(url, data, options);
}
