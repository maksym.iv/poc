const getPaginationList = (currentPage, newsPerPage, options) => {
  const indexOfLastCard = currentPage * newsPerPage;
  const indexOfFirstCard = indexOfLastCard - newsPerPage;
  return options && [...options].slice(indexOfFirstCard, indexOfLastCard);
};

export default getPaginationList;
