import { Layout } from 'antd';
import { Header } from 'components/Header';
import { Footer } from 'components/Footer';
import { SessionProvider } from "next-auth/react"

import '../styles/globals.scss';

const PocApp = ({ Component, pageProps }) => (
  <SessionProvider session={pageProps.session}>
    <Layout className='layout'>
      <Header />
      <Layout.Content>
        <Component {...pageProps} />
      </Layout.Content>
      <Footer>Footer</Footer>
    </Layout>
  </SessionProvider>
)

export default PocApp;
