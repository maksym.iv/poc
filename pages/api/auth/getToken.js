import { users } from 'mocks';

export default async function (req, res) {
    const { token } = req.query;
    const user = users.find(user => user.token === token);

    if (!user) {
        res.json({ errorMessage: "User doesn't exist" });

    } else {
        res.status(200).json(user);
    }
}