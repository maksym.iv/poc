import NextAuth from 'next-auth'
import CredentialsProvider from "next-auth/providers/credentials"
import api from 'api';

function MyAdapter(client, options = {}) {
  return {
    async updateUser(user) {
      try {
        console.log('user :>> ', user);
      } catch (err) {
        console.log(err);
        return;
      }
    },
  }
}

const options = {
  providers: [
    CredentialsProvider({
      id: 'credentials',
      name: 'Credentials',

      credentials: {
        email: { label: "email", type: "text" },
        password: { label: "password", type: "password" }
      },
      authorize: async (credentials) => {
console.log('credentials :>> ', credentials);
        try {
          const user = await api.logInUser(credentials)

          if (!user?.errorMessage) {
            return user
          } 
        } catch (e) {
          const errorMessage = e.response.data.message
          // Redirecting to the login page with error messsage in the URL
          throw new Error(errorMessage + '&email=' + credentials.email)
        }
      }
    })
  ],
  callbacks: {
    async jwt({ token, user, account }) {
      if (user) token.user = user;

      return token
    },

    async session({ session, token }) {
      // console.log('session, token :>> ', session, token);
      // session.user.accessToken = token.accessToken
      // session.user.refreshToken = token.refreshToken
      // session.user.accessTokenExpires = token.accessTokenExpires
      session.user = token.user;
      session.user.id = token.id;
      return session
    },
    redirect: async (url, baseUrl) => {
      // return Promise.resolve('http://localhost:3000')
    
      // Allows relative callback URLs
      // if (url.startsWith("/")) return `${baseUrl}${url}`
      // // Allows callback URLs on the same origin
      // else if (new URL(url).origin === baseUrl) return url
      return 'http://localhost:3000';
    }
  },
  // session: {
  //     jwt: true,
  // },
  pages: {
      signIn: "/auth/login",
      newUser: null,
  },
  // adapter: MyAdapter(),
  secret: 'test',
  jwt: {
    // A secret to use for key generation - you should set this explicitly
    // Defaults to NextAuth.js secret if not explicitly specified.
    // secret: 'INp8IvdIyeMcoGAgFGoA61DdBglwwSqnXJZkgz8PSnw',
    secret: 'test',
    encryption: true
  }
}

export default (req, res) => NextAuth(req, res, options);
