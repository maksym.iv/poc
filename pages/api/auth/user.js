import { users } from 'mocks';

export default async function (req, res) {
    const { username, password } = req.query;

    const user = users.find(user => user.username === username);

    if (!user) {
        res.json({ errorMessage: "User doesn't exist" });
    };
    
    if (user) {
        if (user.password === password) {
            res.status(200).json(user);

        } else {
            res.json({ errorMessage: "Invalid password!" });

        };
    };
};