import { users } from 'mocks';

export default async function (req, res) {
    const { email, username } = req.query;

    const isExistEmail = users.find(user => user.email === email);
    const isExistName = users.find(user => user.username === username);

    if (isExistEmail || isExistName) {
        res.json({ errorMessage: "User already exist" });
    } else {

        users.push(req.query);
        res.json({ data: req.query });
    }
    
    // if (user) {
    //     if (user.password === password) {
    //         res.status(200).json(user);

    //     } else {
    //         res.json({ errorMessage: "Invalid password!" });

    //     };
    // };
};