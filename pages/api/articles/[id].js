import { articles } from 'mocks';

export default function handler(req, res) {
    const { id } = req.query;
    const article = articles.find(plan => plan.id.toString() === id);
    if (article) {
        res.status(200).json(article);
    } else {
        res.status(404).json({ error: 'Data not found' })
    }
}
