import { plans } from 'mocks';

export default function handler(req, res) {
    const { name } = req.query;
    const plan = plans.find(plan => plan.name.toLowerCase() === name);
    res.status(200).json(plan);
}