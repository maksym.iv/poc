import React from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
import api from 'api';
import { Row, Col } from 'antd';
import { LeftOutlined, LockOutlined } from '@ant-design/icons'
import Image from "next/image";
import defaultImage from "public/images/defaultImage.png";
import routes from 'constant/routes';
import { SubscribePopup } from 'components/SubscribePopup';

function Article({ currentArticle }) {

  const toSubscribePage = () => Router.push(routes.subscribe);

  return (
    <Row className="article">
      <Col className="articleNav">
        <button className="backImg" onClick={Router.back}>
          <LeftOutlined />
          back
        </button>
      </Col>
      <Col className="articleContent">
        <div className="imageContent">
          <Image src={defaultImage} width="100%" height={44} layout="responsive" alt='article' placeholder='blur' />
          {currentArticle?.isPrivate ? <div className="isPrivate"><LockOutlined width={16} height={21} /></div> : <div />}
        </div>

        <h2>{currentArticle?.title}</h2>
        <p>{currentArticle?.text}</p>
        {currentArticle?.isPrivate ?
          <SubscribePopup
            description='Subscribe to unlock the rest of the article.'
            btnText='Subscribe'
            clickHandler={toSubscribePage}
          />
          : <div />}
      </Col>
    </Row>
  );
};

Article.propTypes = {
  currentArticle: PropTypes.object,
};

export const getStaticProps = async (context) => {
  const id = context.params.id;
  const currentArticle = await api.getCurrentArticle(id);

  return {
    props: { currentArticle }
  }
};

export const getStaticPaths = async () => {
  const data = await api.getArticles(true);

  const paths = [...data].map((item) => ({
    params: { id: item.id.toString() },
  }));

  return {
    paths,
    fallback: false
  };
};

export default Article;