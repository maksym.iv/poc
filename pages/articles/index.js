import React, { useState } from 'react';
import { Row, Col, Input, Space } from 'antd';
import PropTypes from "prop-types";
import api from 'api';
import defaultImage from "../../public/images/defaultImage.png";
import Image from "next/image";
import Link from "next/link";
import routes from "../../constant/routes";

const Articles = ({ articles }) => {
  const [searchField, setSearchField] = useState('');

  const onSearch = value => setSearchField(value);

  const filteredArticles = articles.filter(article => {

    const byTitle = article.title
      .toLowerCase()
      .includes(searchField.toLowerCase());

    const byText = article.text
      .toLowerCase()
      .includes(searchField.toLowerCase());

    return byTitle || byText;
  });

  return (
    <Row className='articles' justify='center'>
      <Col xs={24}>
        <h1>Articles</h1>
        <Space className="search-block" direction="vertical">
          <Input.Search allowClear placeholder="input search text" onSearch={onSearch} style={{ width: 260 }} />
        </Space>
      </Col>
      <Col className="articles-list" xs={24}>
        {filteredArticles.map(item =>
          <Col xs={24} sm={12} md={6} className="articles-item" key={item.id}>
            <Image src={defaultImage} alt='new-news-image' />
            <h4>{item.title}</h4>
            <p>
              {`${item.text} `}
              <Link className='learnMoreBtn' href={`${routes.articles}/${item.id}`} passHref>
                <a>Learn more</a>
              </Link>
            </p>
          </Col>
        )}
      </Col>
    </Row>
  )
};

Articles.propTypes = {
  articles: PropTypes.arrayOf(PropTypes.object)
};

export const getServerSideProps = async () => {
  const articles = await api.getArticles(true);

  return {
    props: { articles }
  }
};

export default Articles;