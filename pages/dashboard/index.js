import React from 'react';
import PropTypes from "prop-types";
import { Row, Col, Button, Pagination } from 'antd';
import Router from 'next/router';
import _get from 'lodash/get';
import { Paper } from 'hoc/Paper';
import { UserBlock } from 'components/UserBlock';
import { dataTable } from 'mocks';
import routes from 'constant/routes';
import { signOut, getSession } from 'next-auth/react';

function Dashboard({ user }) {

  const updateUserInfo = (values) => { }

  return (
    <Row className='dashboard'>
      <Col className='userInfo' xs={{ span: 8, offset: 1 }} md={{ span: 9, offset: 1 }}>
        <UserBlock removeHandler={signOut} user={user} updateUserInfo={updateUserInfo} />
      </Col>
      <Col className='billingInfo' xs={{ span: 15, offset: 1 }} md={{ span: 13, offset: 1 }}>
        <Paper customClass='billing'>
          <div>
            <h5>Billing</h5>
            <p>Billing history</p>
          </div>
          <div className="billingTable">
            <table>
              <thead>
                <tr>
                  <th>Next invoice issue date</th>
                  <th>Invoice total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Feb 24, 2022</td>
                  <td>16$</td>
                </tr>
                <tr>
                  <td>Feb 24, 2022</td>
                  <td>16$</td>
                </tr>
              </tbody>
            </table>
          </div>
          <Pagination size="small" total={50} />
          <Button onClick={() => Router.push('subscribe/premuim')} type="text" className='upgradeBtn'>Upgrade plan to Premium</Button>
        </Paper>
      </Col>
    </Row>
  );
};

Dashboard.propTypes = {
  user: PropTypes.object
};

export const getServerSideProps = async (context) => {
  const session = await getSession(context);
  const user = _get(session, 'user', {});

  if (!session) {
    return {
      redirect: {
        destination: routes.login,
        permanent: false
      }
    }
  };

  return {
    props: { user }
  };
}

export default Dashboard;
