import Link from 'next/link';

function Custom404() {
  return <div className="error-page">
    <h1>404 - Page Not Found</h1>
    <div className="backToHome">
      <Link href='/'>
        to Home
      </Link>
    </div>
  </div>
}

export default Custom404;
