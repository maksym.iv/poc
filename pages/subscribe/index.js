import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';
import { PlanBlock } from 'components/PlanBlock';
import api from 'api';

function Subscribe({ plans }) {

  return (
    <Row className='subscribe' justify='center'>
      <Col xs={24}>
        <h1>Available plans</h1>
      </Col>
      {plans.map(plan => (
        <Col xs={24} md={12} lg={8} key={plan.name}>
          <PlanBlock plan={plan} />
        </Col>
      ))}
    </Row>
  );
}

Subscribe.propTypes = {
  plans: PropTypes.array
};

export const getServerSideProps = async () => {
  const plans = await api.getSubscribePlans();

  return {
    props: { plans }
  }
};

export default Subscribe;