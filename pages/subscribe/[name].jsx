import React, { useReducer, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
import { getSession } from 'next-auth/react';
import Cleave from 'cleave.js/react';
import { Row, Col, Divider, Form, Input, Button } from 'antd';
import { PlanBlock } from 'components/PlanBlock';
import { Loader } from 'components/Loader';
import { Paper } from 'hoc/Paper';
import { formReducer, hasProperty, cleaveOptions } from 'utils/form';
import validation from 'constant/validation';
import api from 'api';
import routes from 'constant/routes';

const initialState = {
  cvc: '',
  expiry: '',
  nameHolder: '',
  cardNumber: '',
}

function Purchase({ currentPlan }) {
  const [formState, setFormState] = useReducer(
    formReducer,
    initialState
  );
  const [type, setType] = useState('default');
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(true);

  const securePage = async () => {
    const session = await getSession();
    !session ? Router.push(routes.login) : setLoading(false);
  };

  useEffect(() => {
    securePage()
  }, []);

  if (loading) return <Loader />;

  const onSubmit = () => {
    console.log('Success:', formState);
  };

  const onValuesChange = (value) => {
    if (hasProperty(value, 'cardNumber')) {
      const cardNumber = value.cardNumber.split(' - ').join('');

      setFormState({ cardNumber })
    } else if (hasProperty(value, 'expiry')) {
      const expiry = value.expiry.split(' ').join('');

      setFormState({ expiry })
    } else {
      setFormState(value)
    }
  };

  const onCreditCardTypeChanged = (type) => setType(type !== 'unknown' ? type : 'default');

  return (
    <Row className='subscribe' justify='center'>
      <Col md={22} lg={16}>
        <Paper customClass='cardPaper'>
          <p>Free trial Premium plan 3 days left</p>
          <Divider />
          <Form
            layout='vertical'
            form={form}
            initialValues={formState}
            onValuesChange={onValuesChange}
            onFinish={onSubmit}
            scrollToFirstError
            className='paymentMethod'
          >
            <Row className='row'>
              <h3>Select payment method</h3>

              <Col xs={24} className='cardNumber'>
                <Form.Item
                  className={`${type}`}
                  rules={validation.cardNumber}
                  label="Card number" name='cardNumber' value={formState.number}>
                  <Cleave
                    className='ant-input icon'
                    placeholder="XXXX XXXX XXXX XXXX"
                    options={{ ...cleaveOptions.cardNumber, onCreditCardTypeChanged }}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} className='holderName'>
                <Form.Item
                  label="Holder Name"
                  name="nameHolder"
                  value={formState.name}
                  rules={validation.nameHolder}
                >
                  <Input style={{ textTransform: "uppercase" }} />
                </Form.Item>
              </Col>

              <Col xs={24} sm={12} className='expiredDate'>
                <Form.Item
                  rules={validation.expiry}
                  label="Expired date" name="expiry" value={formState.expiry}>
                  <Cleave
                    className='ant-input'
                    placeholder="MM / YY"
                    options={cleaveOptions.expiry}
                  />
                </Form.Item>
              </Col>

              <Col xs={24} sm={12} className='cvc'>
                <Form.Item rules={validation.cvc} label="CVC" name="cvc" value={formState.cvc}>
                  <Input.Password maxLength='3' visibilityToggle={false} />
                </Form.Item>
              </Col>

              <Col xs={24}>
                <Button type='primary' htmlType='submit' className='submitBtn'>Purchase now</Button>
              </Col>
            </Row>
          </Form>
        </Paper>
      </Col>

      <Col md={22} lg={8} key={currentPlan.name}>
        <PlanBlock plan={currentPlan} viewMode />
      </Col>
    </Row>
  );
}

Purchase.propTypes = {
  currentPlan: PropTypes.object
};

export const getStaticProps = async (context) => {
  const name = context.params.name;
  const currentPlan = await api.getSubscribePlanByName(name);

  return {
    props: { currentPlan }
  };
};

export const getStaticPaths = async () => {
  const data = await api.getSubscribePlans();

  const paths = data.map((item) => ({
    params: { name: item.name.toString().toLowerCase() },
  }));

  return {
    paths,
    fallback: false,
  };
};

export default Purchase;