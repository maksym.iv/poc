import React, { useState } from 'react';
import Router from 'next/router';
import { getSession } from 'next-auth/react';
import { Form, Input, Button } from 'antd';
import validation from 'constant/validation';
import { Paper } from 'hoc/Paper';
import routes from 'constant/routes';
import api from 'api';
import { SuccessMessage } from 'components/SuccessMessage';

const initialState = {
  username: '',
  email: '',
  password: '',
  phone: '',
  photo: ''
}

function CreateAccount() {
  const [isConfirm, setIsConfirm] = useState(false);
  const [error, setError] = useState('');

  const toLogin = () => Router.push(routes.login);

  const onFinish = async (values) => {
    const creds = { ...initialState, ...values };
    await api.registrationUser(creds)
      .then(({ errorMessage }) => {
        if (errorMessage) {
          setError(errorMessage)
        } else {
          setIsConfirm(true)
        }
      });
  }

  return (
    <Paper customClass='auth'>
      {isConfirm ?
        <SuccessMessage />
        :
        <Form
          className='authorisation'
          name='normal_create_account'
          onFinish={onFinish}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          autoComplete='off'
        >
          <h2>Create New Password</h2>

          <Form.Item
            justify='center'
            name='password'
            rules={validation.password}
            className="passwordItem"
          >
            <Input.Password
              size='large'
              type='password'
              placeholder='Password'
            />
          </Form.Item>

          <Form.Item
            name="confirm"
            justify='center'
            dependencies={['password']}
            rules={validation.confirmPassword}
          >
            <Input.Password
              size='large'
              type='password'
              placeholder='Confirm Password'
            />
          </Form.Item>
          <Form.Item justify='center' className="loginButton">
            <Button type='primary' htmlType='submit' className='login-form-button'>
              Create Account
            </Button>
            {error && <p className='errorMessage'>{error}</p>}
          </Form.Item>
        </Form >
      }
    </Paper>
  );
}

export async function getServerSideProps({ req }) {
  const session = await getSession({ req });
  if (session) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  return {
    props: {},
  };
}

export default CreateAccount;