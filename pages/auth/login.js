
import React, { useState } from 'react';
import Router from 'next/router';
import { Form, Input, Button } from 'antd';
import validation from 'constant/validation';
import { getSession } from 'next-auth/react';
import { Paper } from 'hoc/Paper';
import routes from 'constant/routes';
import { signIn } from "next-auth/react"

function Login() {
  const [error, setError] = useState('');
  const toForgetPassword = () => Router.push(routes.forgot_password);

  const toCreateAccount = () => Router.push(routes.create_account);

  const onFinish = async (values) => {
    await signIn('credentials', { ...values, redirect: false })
      .then(e => {

        if (e.error) {
          setError(e.error)
        } else {
          Router.push(routes.main);
        }
      })
      .catch(e => console.error(e));

  };

  return (
    <Paper customClass='auth'>
      <Form
        className='authorisation'
        name='normal_login'
        onFinish={onFinish}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        autoComplete='off'
      >
        <h2>Welcome back</h2>

        <Form.Item
          justify='center'
          name='username'
          className="emailItem"
          rules={validation.username}
        >
          <Input placeholder='Username' size='large' />
        </Form.Item>

        <Form.Item
          justify='center'
          name='password'
          rules={validation.password}
          className="passwordItem"
        >
          <Input.Password
            size='large'
            type='password'
            placeholder='Password'
          />
        </Form.Item>

        <a className='login-form-forgot' type='button' onClick={toForgetPassword}>
          Forgot password?
        </a>

        <Form.Item justify='center' className="loginButton">
          <Button type='primary' htmlType='submit' className='login-form-button'>
            Log in
          </Button>
          {error && <p className='errorMessage'>{error}</p>}
        </Form.Item>
        <div className="accountBlock">
          <div className="accountMessage">No  account?</div>
          <a className='login-form-forgot' type='button' onClick={toCreateAccount}>
            Create one
          </a>
        </div>
      </Form>
    </Paper>
  );
}

Login.propTypes = {

};

export async function getServerSideProps({ req, res }) {
  const session = await getSession({ req });
  if (session) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  return {
    props: {},
  };
}

export default Login;