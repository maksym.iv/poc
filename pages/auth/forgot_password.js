
import React, { useState } from 'react';
import Router from 'next/router';
import { Form, Input, Button } from 'antd';
import { getSession } from 'next-auth/react';
import validation from 'constant/validation';
import { Paper } from 'hoc/Paper';
import routes from 'constant/routes';
import api from 'api';
import { SuccessMessage } from 'components/SuccessMessage';

function ForgetPassword() {
  const [isConfirm, setIsConfirm] = useState(false);
  const [error, setError] = useState('');

  const toLogin = () => Router.push(routes.login);

  const onFinish = async (creds) => {
    await api.restorePassword(creds)
      .then(({ errorMessage }) => {
        if (errorMessage) {
          setError(errorMessage)
        } else {
          setIsConfirm(true)
        }
      });
  }

  return (
    <Paper customClass='auth'>
      {isConfirm ?
        <SuccessMessage />
        :
        <Form
          className='authorisation'
          name='normal_forget_password'
          onFinish={onFinish}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          autoComplete='off'
        >
          <h2>Forget password</h2>
          <Form.Item
            justify='center'
            name='username'
            rules={validation.username}
            className="emailItem"
          >
            <Input placeholder='Username' size='large' />
          </Form.Item>

          <Form.Item justify='center' className="loginButton">
            <Button type='primary' htmlType='submit' className='login-form-button'>
              Reset password
            </Button>
            {error && <p className='errorMessage'>{error}</p>}
          </Form.Item>
          <div className="accountBlock">
            <div className="accountMessage">Don’t need to reset password?</div>
            <a className='login-form-forgot' type='button' onClick={toLogin}>
              Log in
            </a>
          </div>
        </Form>
      }
    </Paper>

  );
}

export async function getServerSideProps({ req }) {
  const session = await getSession({ req });
  if (session) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  return {
    props: {},
  };
}

export default ForgetPassword;