import Image from 'next/image';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';
import defaultImage from 'public/images/defaultImage.png';
import { Carousel } from 'components/Carousel';
import Link from 'next/link';
import routes from 'constant/routes'
import api from 'api';

function Home({ articles, freshArticles }) {
  return (
    <Row className='home'>
      <Col md={12} xs={24} className="leftBlock">
        <Image width="639" height="639" src={defaultImage} alt='main-image' />
      </Col>
      <Col md={12} xs={24} className="rightBlock">
        <h2>News</h2>
        {freshArticles.map((item) => (
          <div className='newsWrapper' key={item.id}>
            <Image width="113" height="113" src={defaultImage} alt='new-news-image' />
            <div className="teaserContent">
              <h4>{item.title}</h4>
              <p>
                {`${item.text} `}
                <Link className='learnMoreBtn' href={`${routes.articles}/${item.id}`} passHref>
                  <a>Learn more</a>
                </Link>
              </p>
            </div>
          </div>
        ))}
      </Col>
      <Carousel articlesList={articles} />
    </Row>
  );
}

Home.propTypes = {
  freshArticles: PropTypes.arrayOf(PropTypes.object),
  articles: PropTypes.arrayOf(PropTypes.object)
};

export const getServerSideProps = async () => {
  const { articles, freshArticles } = await api.getArticles();

  return {
    props: { articles, freshArticles }
  }
};

export default Home;