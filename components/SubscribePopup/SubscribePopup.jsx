import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import { LockOutlined } from '@ant-design/icons';

function SubscribePopup({ description, btnText, clickHandler }) {
  return (
    <div className="subscriberAccess">
      <div className="subscriberText">
        <LockOutlined width={16} height={21} />
        <p>{description}</p>
      </div>
      <Button type='link' onClick={clickHandler}>
        {btnText}
      </Button>
    </div>
  );
};

SubscribePopup.propTypes = {
  description: PropTypes.string,
  btnText: PropTypes.string,
  clickHandler: PropTypes.func
};

export default SubscribePopup;