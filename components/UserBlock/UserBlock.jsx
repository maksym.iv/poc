import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Button, Avatar, Upload } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Paper } from 'hoc/Paper';
import { getBase64, showPreview } from 'utils/form';

function UserBlock({ removeHandler, user, updateUserInfo }) {
  const [idEditMode, setIsEditMode] = useState(false);
  const [formState, setFormState] = useState({});
  const [form] = Form.useForm();

  useEffect(() => {
    setFormState(user);
  }, [user])

  const onSubmit = (values) => {
    console.log('Success:', values);
    setIsEditMode(false);
    updateUserInfo(values);
  };

  const onValuesChange = (value) => setFormState((prev) => ({ ...prev, ...value }));

  const uploadHandleChange = async (value) => {
    const { file } = value;
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    };

    onValuesChange({ photo: file });
  }

  const renderForm = () =>
  (
    <Form
      layout='vertical'
      form={form}
      initialValues={formState}
      onValuesChange={onValuesChange}
      onFinish={onSubmit}
    >
      <Form.Item>
        <div style={{ display: 'flex' }}>
          <Avatar className='editAvatar' size={88} icon={<UserOutlined />} src={showPreview(formState.photo)} />
          <Upload showUploadList={false} listType='text' className="uploader"
            onChange={uploadHandleChange}>
            <Button>Change</Button>
          </Upload>
        </div>
      </Form.Item>
      <Form.Item label="Name" name="name" value={formState.username}>
        <Input />
      </Form.Item>
      <Form.Item label="Email" name="email" value={formState.email}>
        <Input />
      </Form.Item>
      <Form.Item label="Phone" name="phone" value={formState.phone}>
        <Input />
      </Form.Item>

      <Button type='primary' htmlType='submit' className='submitBtn'>Save</Button>
    </Form>
  );

  return (
    <Paper customClass='profile'>
      {idEditMode ? renderForm() :
        (
          <>
            <Button onClick={() => setIsEditMode(true)} type="text" className='editBtn'>Edit profile</Button>
            <Avatar className='avatar' size={88} icon={<UserOutlined />} src={user.photo} />
            <h3 className='title'>{user.username}</h3>
            <p className='email'>{user.email}</p>
            <p className='email'>{user.phone}</p>
            <Button onClick={removeHandler} type="text" className='logout'>log out</Button>
          </>
        )}
    </Paper>
  );
}

UserBlock.propTypes = {
  user: PropTypes.object.isRequired,
  removeHandler: PropTypes.func.isRequired,
  updateUserInfo: PropTypes.func.isRequired
};

export default UserBlock;