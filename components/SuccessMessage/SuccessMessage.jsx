import React from 'react';
import { Result, Button } from 'antd';
import Router from 'next/router';
import routes from 'constant/routes';

function Success() {
  const moveHome = () => Router.push(routes.main);

  return (
    <div className='authorisation'>
      <h2>Success</h2>
      <Result
        status="success"
        subTitle="Confirm your email to finish registartion"
        extra={[
          <Button onClick={moveHome} type="primary" key="console">
            Go Home
          </Button>
        ]}
      />
    </div>
  );
}

export default Success;