import React from 'react';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

const styles = {
    fontSize: 72,
    textAlign: 'center',
    width: '100%',
    position: "absolute",
    top: '40%'
}

const antIcon = <LoadingOutlined style={styles} spin />;

function Loader() { 
    return <Spin indicator={antIcon} />;
 };

export default Loader;