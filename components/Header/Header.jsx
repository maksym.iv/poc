import React from 'react';
import Router from 'next/router';
import Image from 'next/image';
import Link from 'next/link';
import { Row, Col, Button, Avatar } from 'antd';
import { UserOutlined, SearchOutlined } from '@ant-design/icons'
import _get from 'lodash/get';
import logo from 'public/images/logo.svg';
import routes from 'constant/routes';
import { useSession } from 'next-auth/react';

function Header() {
  const { data: session } = useSession();
  const user = _get(session, 'user', {});

  const showModal = () => Router.push(routes.login);

  const renderUser = () => {
    return user.username ? (
      <Link href={routes.dashboard} passHref>
        <div className='user'>
          <Avatar size="large" icon={<UserOutlined />} src={user?.photo} style={{}} />
          <p>
            {user.username}
          </p>
        </ div>
      </Link>
    ) :
      <Button
        type='primary'
        className='login'
        icon={<UserOutlined />}
        onClick={showModal}
      >
        Sign in
      </Button>
  };

  return (
    <Row className='header'>
      <Col md={12} xs={24} className='logoImg'>
        <Link href={routes.main} passHref>
          <a>
            <Image src={logo} alt="logo" />
          </a>
        </Link>
      </Col>
      <Col md={12} xs={24} className="blockWithButtons">
        <Row justify='end' align="middle">
          <Col>
            {renderUser()}
          </Col>
          <Col>
            <Link href={routes.articles}>
              <SearchOutlined style={{ padding: '10px 20px 10px 0px' }} />
            </Link>
          </Col>
          <Col>
            <Link href={routes.subscribe}>
              Subscribe
            </Link>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default Header;