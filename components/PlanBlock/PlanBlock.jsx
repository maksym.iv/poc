import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import Router from 'next/router';
import { Paper } from 'hoc/Paper';
import routes from 'constant/routes';

function PlanBlock({ plan, viewMode }) {
  const renderDescription = (arr) => arr.map((text, i) => <li key={text + i}>{text}</li>);

  const moveTo = (isFree) => {
    const route = `${routes.subscribe}/${isFree ? 'basic' : plan.name.toLowerCase()}`

    Router.push(route)
  };

  return (
    <Paper customClass='planBlock'>
      <h4>{plan.name}</h4>
      <h1>{plan.price}</h1>
      <div className='descriptions'>
        <ul>
          {renderDescription(plan.descriptions)}
        </ul>
      </div>
      {viewMode ? <div /> :
        <>
          <Button type="link" onClick={() => moveTo(false)}>
            <a className='subscribeBtn'>Subscribe</a>
          </Button>
          {plan.name !== 'Basic' ?
            <Button type="link" onClick={() => moveTo(true)}>
              <a className='freeBtn'>
                Free trial
              </a>
            </Button>
            : <div style={{height: 55}}/>
          }
        </>
      }
    </Paper>
  );
}

PlanBlock.defaultProps = {
  viewMode: false
}

PlanBlock.propTypes = {
  plan: PropTypes.object,
  viewMode: PropTypes.bool
};

export default PlanBlock;

