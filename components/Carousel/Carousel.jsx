import React, { useState } from 'react';
import Image from 'next/image';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { Row, Col, Pagination } from 'antd';
import getPaginationList from 'utils/getPaginationList';
import routes from 'constant/routes';

import defaultImage from 'public/images/defaultImage.png';

function Carousel({ articlesList }) {
  const [page, setPage] = useState(1)
  const timesPerPage = 4;
  const totalLength = articlesList.length
  const list = getPaginationList(
    page,
    timesPerPage,
    articlesList
  );

  return (
    <div className="paginationBlock">
      <Row className='paginationList'>
        {list.map(item =>
          <Col xs={12} sm={12} md={6} key={item.id}>
            <Image src={defaultImage} alt="old-news-image" />
            <h4>{item.title}</h4>
            <p>
              {item.text}
              <Link href={`${routes.articles}/[id]`} as={`${routes.articles}/${item.id}`}> Learn more</Link>
            </p>
          </Col>
        )}
      </Row>

      <Pagination defaultPageSize={4} current={page} size='small' total={totalLength} onChange={setPage} />
    </div>
  );
}

Carousel.propTypes = {
  articlesList: PropTypes.array
};

export default Carousel;