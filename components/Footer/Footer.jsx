import React from 'react';
import Image from 'next/image';
import logo from 'public/images/logoname.svg';

function Footer() {
  return (
    <div className='footer'>
      <Image src={logo} alt="logo" />
    </div>
  );
}

export default Footer;