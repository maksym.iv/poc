import React from 'react';
import PropTypes from 'prop-types';

const Paper = ({ children, customClass }) => (<div className={`paper ${customClass}`}>{children}</div>);

Paper.propTypes = {
    children: PropTypes.node,
    customClass: PropTypes.string
};

export default Paper;