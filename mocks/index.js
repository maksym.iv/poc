const articles = [
  { id: 'news-new-1', title: 'Pacific fresh Ocean 1', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-new-2', title: 'Pacific fresh Ocean 2', text: 'The Pacific Ocean is the largest and deepest of Earth\'s five oceanic divisions. It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-new-3', title: 'Pacific fresh Ocean 3', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: false },
  { id: 'news-old-1', title: 'Pacific Ocean 1', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-old-2', title: 'Pacific Ocean 2', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-old-3', title: 'Pacific Ocean 3', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-old-4', title: 'Pacific Ocean 4', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-old-5', title: 'Pacific Ocean 5', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-old-6', title: 'Pacific Ocean 6', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-old-7', title: 'Pacific Ocean 7', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-old-8', title: 'Pacific Ocean 8', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true },
  { id: 'news-old-9', title: 'Pacific Ocean 9', text: 'It extends from the Arctic Ocean in the north to the Southern Ocean.', isPrivate: true }
]

const dataTable = {
  dataSource: [
    {
      key: '1',
      name: 'Feb 24, 2022',
      age: '16$'
    },
    {
      key: '2',
      name: 'Feb 24, 2022',
      age: '16$'
    },
  ],
  columns: [
    {
      title: 'Next invoice issue date',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Invoice total',
      dataIndex: 'age',
      key: 'age',
    }
  ]
}

const plans = [
  { name: 'Basic', price: 'Free', descriptions: ['10 articles', 'Cloud storage 100mb', 'Cloud storage 100mb', 'Cloud storage 100mb'], access: 2 },
  { name: 'Standart', price: '16$', descriptions: ['10 articles', 'Cloud storage 100mb', 'Cloud storage 100mb', 'Cloud storage 100mb'], access: 3 },
  { name: 'Premuim', price: '24$', descriptions: ['10 articles', 'Cloud storage 100mb', 'Cloud storage 100mb', 'Cloud storage 100mb'], access: 4 }
]

const users = [
  {   username: 'test', email: 'test@test.com', phone: '9823674827', password: 'test', photo: 'https://i.pinimg.com/originals/87/da/03/87da035b88d7f76a5abbd3028d082df5.jpg' },
  {   username: 'parker', email: 'parker@gmail.com',   password: 'spider', photo: 'https://i.pinimg.com/originals/87/da/03/87da035b88d7f76a5abbd3028d082df5.jpg' },
  {   username: 'tesla', email: 'elon@tesla.com', phone: '31298473212',   password: 'tesla', photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9LPJJlt5GqqjTfr4Z3t1P01dHjY-LBygekA&usqp=CAU' },

]

export { articles, dataTable, plans, users }