export default {
    main: '/',
    articles: '/articles',
    subscribe: '/subscribe',
    dashboard: '/dashboard',
    login: '/auth/login',
    forgot_password: '/auth/forgot_password',
    create_account: '/auth/create_account'
}