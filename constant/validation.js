export default {
  email: [
    {
      type: 'email',
      message: 'The input is not valid E-mail!',
    },
    {
      required: true,
      message: 'Please input your E-mail!',
    }
  ],
  username: [
    {
      required: true,
      message: 'Please input your Username!'
    }
  ],
  password: [
    { required: true, message: 'Please input your Password!' }
  ],
  confirmPassword: [
    {
      required: true,
      message: 'Please confirm your password!',
    },
    ({ getFieldValue }) => ({
      validator(_, value) {
        if (!value || getFieldValue('password') === value) {
          return Promise.resolve();
        }
        return Promise.reject(new Error('The two passwords that you entered do not match!'));
      },
    }),
  ],
  cardNumber: [
    {
      required: true,
      message: 'Please input your Credit Card number!',
    },
    {
      min: 25,
      message: 'Wrong Credit Card number!',
    }
  ],
  nameHolder: [
    {
      required: true,
      message: 'Please input card holder name!',
    },
    {
      min: 5,
      message: 'Too short card holder name!',
    }
  ],
  expiry: [
    {
      required: true,
      message: 'Please input expired date!',
    },
    {
      min: 7,
      message: 'Wrong date!',
    }
  ],
  cvc: [
    {
      required: true,
      message: 'Please input cvc password!',
    },
    {
      min: 3,
      message: 'Needs three digits!',
    }
  ]
}