import { get } from './axios'

export default {
    logInUser: async (creds) => {
        const { data } = await get('/auth/user', creds);
        return data;
    },
    registrationUser: async (creds) => {
        const { data } = await get('/api/auth/registration', creds);
        return data;
    },
    restorePassword: async (creds) => {
        // const { data } = await get('/api/auth/user', creds);
        const data = { message: 'success' };
        return data;
    },
    getArticles: async (full = false) => {
        const { data } = await get(`/articles`);
        if (full) return data;

        const freshArticles = data.splice(0, 3);
        return { articles: data, freshArticles }
    },
    getCurrentArticle: async (id) => {
        const { data } = await get(`/articles/${id}`);
        return data;
    },
    getSubscribePlans: async () => {
        const { data } = await get(`/plans`);
        return data;
    },
    getSubscribePlanByName: async (name) => {
        const { data } = await get(`/plans/${name}`);
        return data;
    },
}